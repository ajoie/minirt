/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 17:42:10 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 14:52:42 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "error.h"
# include "minirt.h"
# include "get_next_line.h"
# include "libft.h"
# include <fcntl.h>
# include <stdlib.h>

/*
** ============================== parser =======================================
*/
t_error		parser				(t_scene *scene, int argc, char *argv[]);
t_error		parse_line			(t_scene *scene, char **sv);

/*
** =========================== parser tools ====================================
*/
int			free_split			(char **rr);
t_error		is_file_name_valid	(const char *path);
t_error		parse_doubl			(const char *str, double *num);
t_error		parse_color			(const char *str, t_color *color);
t_error		parse_vectr			(const char *str, t_vectr *vectr);
int			dummy_atoi			(const char *str);
int			clear_obj			(void *a, void *b);

/*
** ========================== object parsers ===================================
*/
t_error		resolution_parse	(t_scene *scene, char **splited_values);
t_error		ambient_parse		(t_scene *scene, char **splited_values);
t_error		camera_parse		(t_scene *scene, char **splited_values);
t_error		light_parse			(t_scene *scene, char **splited_values);
t_error		sphere_parse		(t_scene *scene, char **splited_values);
t_error		plane_parse			(t_scene *scene, char **splited_values);
t_error		square_parse		(t_scene *scene, char **splited_values);
t_error		cylinder_parse		(t_scene *scene, char **splited_values);
t_error		triangle_parse		(t_scene *scene, char **splited_values);
t_error		antialiasing_parse	(t_scene *scene, char **splited_values);

#endif
