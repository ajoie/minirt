/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processor.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 17:50:59 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/06 14:15:19 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROCESSOR_H
# define PROCESSOR_H

# include "minirt.h"
# include "libft.h"
# include "error.h"
# include "vector.h"
# include <math.h>
# include <mlx.h>

/*
** ============================= main things ===================================
*/
t_error	draw(t_scene *scene);
t_error	processor(t_scene *scene);

void	get_pixel_color(t_scene *scene, t_intersection *i, int x, int y);
void	get_pixel_color_aa(t_scene *scene, t_intersection *i, int x, int y);
void	get_ray(t_intersection *i, t_scene *scene, double x, double y);
int		get_nearest_object(t_scene *scene, t_intersection *i);

/*
** =============================== camera ======================================
*/
t_vectr	camera_lookat(t_vectr v, t_vectr dir);

/*
** ================================ hooks ======================================
*/
int		color_to_int(t_color c);
void	color_add(t_intersection *i, t_light l, double k);
void	color_apply(t_intersection *i);
t_color	color_plus(t_color a, t_color b);
t_color	color_div(t_color a, int d);

/*
** =============================== hooks =======================================
*/
int		hook_keypress(int keycode, t_scene *scene);
int		hook_close_and_free(t_scene *scene);
int		hook_image_put(t_scene *s);
void	hook_cam_next(t_scene *s);
void	hook_cam_prev(t_scene *s);

/*
** ================================ mlx ========================================
*/
void	free_mlx_cams(t_scene *s);
void	mlx_rt_pixel_put(t_data *data, int x, int y, int color);
t_error	mlx_rt_window_init(t_scene *s);
t_error	mlx_rt_init(t_scene *s);
t_error	mlx_rt_put_image(t_scene *s, t_camera *cam);
t_error	mlx_rt_images_init(t_scene *s);
/*
** =============================== light =======================================
*/
void	light(t_scene *scene, t_intersection *i);
int		light_shadows(t_scene *scene, t_intersection *i, t_light *light);
void	light_diffuseness(t_intersection *i, t_light *light);
void	light_glassy(t_intersection *i, t_light *light);

/*
** ================================ bmp ========================================
*/
void	bmp_gen_images(t_scene *s);
void	bmp_gen(unsigned char *image, int height, int width, char *path);
void	bmp_file_header(int fd, int height, int stride);
void	bmp_info_header(int fd, int height, int width);

#endif
