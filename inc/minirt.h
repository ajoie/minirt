/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 14:35:15 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 17:32:47 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H

# include "libft.h"
# include "error.h"
# include "vector.h"
# include <stdlib.h>
# include <math.h>

# define L_AMBIENT 0
# define L_POINT   1

typedef struct	s_color
{
	float		r;
	float		g;
	float		b;
}				t_color;

typedef struct	s_ray
{
	t_vectr		origin;
	t_vectr		direction;
}				t_ray;

typedef struct	s_ambient
{
	t_color		color;
	double		ratio;
}				t_ambient;

typedef struct	s_data
{
	void		*img;
	char		*addr;
	int			bpp;
	int			ll;
	int			end;
}				t_data;

typedef struct	s_camera
{
	t_vectr		point;
	t_vectr		normale;
	double		angle;
	double		fov;
	int			*raw_img;
	t_data		mlx_img;
}				t_camera;

typedef struct	s_light
{
	t_vectr		point;
	t_color		color;
	double		ratio;
	int			type;
}				t_light;

typedef struct	s_sphere
{
	t_vectr		point;
	double		diameter;
}				t_sphere;

typedef struct	s_plane
{
	t_vectr		point;
	t_vectr		normale;
}				t_plane;

typedef struct	s_square
{
	t_vectr		point;
	t_vectr		normale;
	double		sidesize;
}				t_square;

typedef struct	s_cylinder
{
	t_vectr		point;
	t_vectr		normale;
	double		diameter;
	double		height;
}				t_cylinder;

typedef struct	s_triangle
{
	t_vectr		a;
	t_vectr		b;
	t_vectr		c;
}				t_triangle;

typedef struct	s_object
{
	void		*data;
	int			(*intersect)(void *i);
	t_color		color;
}				t_object;

typedef struct	s_intersection
{
	t_ray		ray;
	t_vectr		point;
	t_vectr		normale;
	double		t;
	t_color		color;
	t_color		light;
	t_object	*obj;
	t_object	*tmp_obj;
}				t_intersection;

typedef struct	s_window
{
	void		*win_ptr;
	void		*mlx_ptr;
	int			h;
	int			w;
}				t_window;

typedef struct	s_scene
{
	t_window	window;
	t_list		*cams;
	t_camera	*cam;
	t_list		*objects;
	t_list		*lights;
	int			active_cam_id;
	double		aa;
	int			has_aa;
	int			has_amb;
	int			has_res;
	int			save;
	int			parse_err;
}				t_scene;

t_error			scene_init(t_scene *scene);
void			scene_free(t_scene *scene);

#endif
