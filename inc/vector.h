/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 20:17:11 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 17:55:24 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_H
# define VECTOR_H

typedef struct	s_vectr
{
	double		i;
	double		j;
	double		k;
}				t_vectr;

t_vectr			p(t_vectr a, t_vectr b);
t_vectr			m(t_vectr a, t_vectr b);
t_vectr			x(t_vectr a, double b);
t_vectr			d(t_vectr a, double b);
t_vectr			norm(t_vectr a);
t_vectr			vxv(t_vectr a, t_vectr b);
double			dot(t_vectr a, t_vectr b);
double			length(t_vectr a);
int				is_same(t_vectr a, t_vectr b);
t_error			is_normalized(t_vectr a);

#endif
