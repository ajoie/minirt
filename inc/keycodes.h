/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keycodes.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 10:17:36 by ajoie             #+#    #+#             */
/*   Updated: 2021/02/19 12:55:14 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYCODES_H
# define KEYCODES_H
# ifdef __linux__
#  define KEY_ESC	65307
#  define KEY_Q		113
#  define KEY_N		110
#  define KEY_P		112
# else
#  define KEY_ESC	53
#  define KEY_Q		12
#  define KEY_N		45
#  define KEY_P		35
# endif
#endif
