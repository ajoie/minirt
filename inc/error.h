/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 16:52:04 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 18:30:53 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_H
# define ERROR_H

typedef enum	e_error_codes {
	ok,
	err_bad_number_of_args,
	err_bad_file_name,
	err_invalid_file,
	err_malloc,
	err_bad_argument
}				t_error;

char *error_to_string(t_error e);

#endif
