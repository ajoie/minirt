/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_parser.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/06 12:12:26 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 18:56:45 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "parser.h"

int			dummy_atoi(const char *str)
{
	unsigned long int	res;
	int					len;

	res = 0;
	len = 0;
	while (*str != 0 && *str != ',' && *str != '.')
	{
		if (!ft_isdigit(*str))
			return (-1);
		res *= 10;
		res += (*str++) - '0';
		++len;
	}
	if (len == 0 || (res > 9223372036854775807))
		return (-1);
	return (res);
}

double		sub(double num)
{
	while (num >= 1)
		num /= 10;
	return (num);
}

t_error		parse_doubl(const char *str, double *num)
{
	double	full;
	double	frac;
	int		k;

	if (str == NULL)
		return (err_invalid_file);
	k = 1;
	frac = 0;
	if (*str == '-' && *str++)
		k = -1;
	if ((full = dummy_atoi(str)) == -1)
		return (err_invalid_file);
	if ((str = ft_strchr(str, '.')))
	{
		if ((frac = dummy_atoi(++str)) == -1)
			return (err_invalid_file);
	}
	*num = (full + sub(frac)) * k;
	return (ok);
}
