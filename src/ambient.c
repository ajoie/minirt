/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ambient.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:22:49 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 18:54:27 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"

t_error		ambient_parse(t_scene *scene, char **splited_values)
{
	t_list	*new_list;
	t_light	*new_light;

	if (splited_values[1] == NULL || scene->has_amb++ != 0)
		return (err_invalid_file);
	if ((new_light = (t_light *)malloc(sizeof(t_light))) == NULL)
		return (err_malloc);
	if (parse_doubl(splited_values[1], &new_light->ratio) != ok ||
		parse_color(splited_values[2], &new_light->color) != ok ||
		new_light->ratio < 0 || new_light->ratio > 1 + 1e-8)
		return (clear_obj(new_light, NULL) + err_invalid_file);
	new_light->type = L_AMBIENT;
	if ((new_list = ft_lstnew(new_light)) == NULL)
		return (clear_obj(new_light, NULL) + err_malloc);
	ft_lstadd_back(&(scene->lights), new_list);
	return (ok);
}
