/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 22:53:39 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/06 11:06:48 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"
#include "vector.h"
#include <math.h>

static int	intersect(void *vi)
{
	t_intersection	*i;
	t_sphere		*s;
	double			arr[6];
	t_vectr			n;

	i = vi;
	s = (t_sphere *)i->obj->data;
	arr[0] = 1;
	arr[1] = 2 * dot(i->ray.direction, m(i->ray.origin, s->point));
	n = m(i->ray.origin, s->point);
	arr[2] = dot(n, n) - pow(s->diameter / 2, 2);
	arr[3] = arr[1] * arr[1] - 4 * arr[0] * arr[2];
	if (arr[3] < 0)
		return (0);
	arr[4] = (-arr[1] - sqrt(arr[3])) / (2 * arr[0]);
	arr[5] = (-arr[1] + sqrt(arr[3])) / (2 * arr[0]);
	if (arr[5] < 0)
		return (0);
	if (arr[4] > 0)
		i->t = arr[4];
	else
		i->t = arr[5];
	i->point = p(i->ray.origin, x(i->ray.direction, i->t));
	i->normale = norm(m(i->point, s->point));
	return (1);
}

t_error		sphere_parse(t_scene *scene, char **splited_values)
{
	t_list		*new_list;
	t_sphere	*new_sphere;
	t_object	*new_object;

	if ((new_sphere = (t_sphere *)malloc(sizeof(t_sphere))) == NULL)
		return (err_malloc);
	if ((new_object = (t_object *)malloc(sizeof(t_object))) == NULL)
		return (clear_obj(new_sphere, NULL) + err_malloc);
	if (parse_vectr(splited_values[1], &new_sphere->point) != ok ||
	parse_doubl(splited_values[2], &new_sphere->diameter) != ok ||
	parse_color(splited_values[3], &new_object->color) != ok)
		return (clear_obj(new_sphere, new_object) + err_invalid_file);
	new_object->data = new_sphere;
	new_object->intersect = &intersect;
	if ((new_list = ft_lstnew(new_object)) == NULL)
		return (clear_obj(new_sphere, new_object) + err_malloc);
	ft_lstadd_back(&(scene->objects), new_list);
	return (ok);
}
