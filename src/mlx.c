/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 11:40:24 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 20:26:07 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"
#include "mlx.h"

void		mlx_rt_pixel_put(t_data *data, int x, int y, int color)
{
	char	*dst;

	dst = data->addr + (y * data->ll + x * (data->bpp / 8));
	*(unsigned int *)dst = color;
}

t_error		mlx_rt_window_init(t_scene *s)
{
	s->window.win_ptr = mlx_new_window(s->window.mlx_ptr, s->window.w,
							s->window.h, "miniRT");
	mlx_rt_images_init(s);
	mlx_hook(s->window.win_ptr, 2, 1, hook_keypress, s);
	mlx_hook(s->window.win_ptr, 17, 1, hook_close_and_free, s);
	mlx_loop_hook(s->window.mlx_ptr, hook_image_put, s);
	mlx_loop(s->window.mlx_ptr);
	return (ok);
}

t_error		mlx_rt_init(t_scene *s)
{
	int sizex;
	int sizey;

	s->window.mlx_ptr = mlx_init();
	mlx_get_screen_size(s->window.mlx_ptr, &sizex, &sizey);
	if (sizex < s->window.w)
		s->window.w = sizex;
	if (sizey < s->window.h)
		s->window.h = sizey;
	return (ok);
}

t_error		mlx_rt_put_image(t_scene *s, t_camera *cam)
{
	int x;
	int y;

	x = 0;
	while (x < s->window.w)
	{
		y = 0;
		while (y < s->window.h)
		{
			mlx_rt_pixel_put(&cam->mlx_img, x, s->window.h - 1 - y,
								cam->raw_img[s->window.w * y + x]);
			++y;
		}
		++x;
	}
	return (ok);
}

t_error		mlx_rt_images_init(t_scene *s)
{
	t_list		*ptr;
	t_camera	*cam;

	ptr = s->cams;
	while (ptr != NULL)
	{
		cam = ptr->content;
		cam->mlx_img.img = mlx_new_image(s->window.mlx_ptr, s->window.w,
											s->window.h);
		cam->mlx_img.addr = mlx_get_data_addr(cam->mlx_img.img,
						&cam->mlx_img.bpp, &cam->mlx_img.ll, &cam->mlx_img.end);
		mlx_rt_put_image(s, cam);
		ptr = ptr->next;
	}
	return (ok);
}
