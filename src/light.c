/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:23:10 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 19:11:03 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "processor.h"
#include "minirt.h"
#include <stdio.h>

void		light(t_scene *scene, t_intersection *i)
{
	t_list		*ptr_l;
	t_light		*light;

	ptr_l = scene->lights;
	while (ptr_l != NULL)
	{
		light = ptr_l->content;
		if (light->type == L_AMBIENT)
			color_add(i, *light, 1);
		else if (!light_shadows(scene, i, light))
		{
			light_diffuseness(i, light);
			light_glassy(i, light);
		}
		ptr_l = ptr_l->next;
	}
}

int			light_shadows(t_scene *scene, t_intersection *i, t_light *light)
{
	t_intersection	s;

	s.ray = (t_ray){light->point, norm(m(i->point, light->point))};
	if (get_nearest_object(scene, &s) && s.obj == i->obj &&
		is_same(s.point, i->point) && is_same(s.normale, i->normale))
		return (0);
	return (1);
}

void		light_diffuseness(t_intersection *i, t_light *light)
{
	t_vectr	l;
	t_vectr	n;
	double	n_dot_l;

	l = m(light->point, i->point);
	n = i->normale;
	n_dot_l = dot(n, l);
	if (n_dot_l > 0)
		color_add(i, *light, n_dot_l / (length(n) * length(l)));
}

void		light_glassy(t_intersection *i, t_light *light)
{
	t_vectr	v;
	t_vectr	r;
	t_vectr	l;
	double	r_dot_v;

	l = m(light->point, i->point);
	v = (t_vectr){	-i->ray.direction.i,
					-i->ray.direction.j,
					-i->ray.direction.k};
	r = m(x(x(i->normale, 2), dot(i->normale, l)), l);
	r_dot_v = dot(r, v);
	if (r_dot_v > 0)
		color_add(i, *light, pow(r_dot_v / (length(r) * length(v)), 40));
}

t_error		light_parse(t_scene *scene, char **splited_values)
{
	t_list	*new_list;
	t_light	*new_light;

	if ((new_light = (t_light *)malloc(sizeof(t_light))) == NULL)
		return (err_malloc);
	if (parse_vectr(splited_values[1], &new_light->point) != ok ||
		parse_doubl(splited_values[2], &new_light->ratio) != ok ||
		parse_color(splited_values[3], &new_light->color) != ok ||
		new_light->ratio < 0 || new_light->ratio > 1 + 1e-8)
		return (clear_obj(new_light, NULL) + err_invalid_file);
	new_light->type = L_POINT;
	if ((new_list = ft_lstnew(new_light)) == NULL)
		return (clear_obj(new_light, NULL) + err_malloc);
	ft_lstadd_back(&(scene->lights), new_list);
	return (ok);
}
