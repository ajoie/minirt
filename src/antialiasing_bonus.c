/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   antialiasing_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 13:12:25 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/04 18:56:59 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"
#include "parser.h"

void		get_pixel_color_aa(t_scene *scene, t_intersection *i, int x, int y)
{
	double	aa[2];
	int		k;
	t_color	res;

	k = 0;
	aa[0] = 0;
	res = (t_color){0, 0, 0};
	while (aa[0] < 1)
	{
		aa[1] = 0;
		while (aa[1] < 1 && k++ >= 0)
		{
			i->light = (t_color){0, 0, 0};
			i->color = (t_color){0, 0, 0};
			get_ray(i, scene, x + aa[0], y + aa[1]);
			i->obj = NULL;
			if (get_nearest_object(scene, i))
				light(scene, i);
			color_apply(i);
			res = color_plus(res, i->color);
			aa[1] += 1 / scene->aa;
		}
		aa[0] += 1 / scene->aa;
	}
	i->color = color_div(res, k);
}

t_error		antialiasing_parse(t_scene *scene, char **splited_values)
{
	if (splited_values[1] == NULL || scene->has_aa++ != 0)
		return (err_invalid_file);
	if (parse_doubl(splited_values[1], &scene->aa) != ok)
		return (err_invalid_file);
	if (scene->aa < 1)
		return (err_invalid_file);
	return (ok);
}
