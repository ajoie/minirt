/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 11:31:12 by ajoie             #+#    #+#             */
/*   Updated: 2021/02/19 13:19:54 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "processor.h"
#include "minirt.h"
#include "libft.h"

int		color_to_int(t_color c)
{
	int		rgb;

	rgb = c.r * 255;
	rgb = (rgb << 8) + c.g * 255;
	rgb = (rgb << 8) + c.b * 255;
	return (rgb);
}

void	color_add(t_intersection *i, t_light l, double k)
{
	i->light.r += l.color.r * l.ratio * k;
	i->light.g += l.color.g * l.ratio * k;
	i->light.b += l.color.b * l.ratio * k;
}

void	color_apply(t_intersection *i)
{
	i->color.r = ft_mind(1, i->color.r * i->light.r);
	i->color.g = ft_mind(1, i->color.g * i->light.g);
	i->color.b = ft_mind(1, i->color.b * i->light.b);
}

t_color	color_plus(t_color a, t_color b)
{
	t_color	res;

	res.r = a.r + b.r;
	res.g = a.g + b.g;
	res.b = a.b + b.b;
	return (res);
}

t_color	color_div(t_color a, int d)
{
	t_color	res;

	res.r = a.r / d;
	res.g = a.g / d;
	res.b = a.b / d;
	return (res);
}
