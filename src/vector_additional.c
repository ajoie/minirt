/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_additional.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 20:05:09 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 19:09:45 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include <math.h>

double	dot(t_vectr a, t_vectr b)
{
	return (a.i * b.i + a.j * b.j + a.k * b.k);
}

double	length(t_vectr a)
{
	return (sqrt(dot(a, a)));
}

t_vectr	norm(t_vectr a)
{
	double	n;
	t_vectr	v;

	n = length(a);
	v.i = a.i / n;
	v.j = a.j / n;
	v.k = a.k / n;
	return (v);
}

t_vectr	vxv(t_vectr a, t_vectr b)
{
	t_vectr v;

	v.i = a.j * b.k - a.k * b.j;
	v.j = a.k * b.i - a.i * b.k;
	v.k = a.i * b.j - a.j * b.i;
	return (v);
}

int		is_same(t_vectr a, t_vectr b)
{
	if (fabs(a.i - b.i) > 1e-10)
		return (0);
	if (fabs(a.j - b.j) > 1e-10)
		return (0);
	if (fabs(a.k - b.k) > 1e-10)
		return (0);
	return (1);
}
