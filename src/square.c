/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   square.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:23:13 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 20:30:53 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"
#include <math.h>

static int	intersect(void *vi)
{
	t_intersection	*i;
	t_square		*sq;
	t_vectr			a;
	double			c;
	double			z;

	i = vi;
	sq = (t_square *)i->obj->data;
	z = dot(i->ray.direction, sq->normale);
	if (z < 1e-8 && z > -1e-8)
		return (0);
	c = -dot(m(i->ray.origin, sq->point), sq->normale);
	i->t = c / z;
	if (i->t < 0)
		return (0);
	i->point = p(i->ray.origin, x(i->ray.direction, i->t));
	a = m(i->point, sq->point);
	if (fabs(a.i) > sq->sidesize / 2 ||
		fabs(a.j) > sq->sidesize / 2 ||
		fabs(a.k) > sq->sidesize / 2)
		return (0);
	i->normale = norm(sq->normale);
	if (dot(i->ray.direction, sq->normale) > 0)
		i->normale = m((t_vectr){0, 0, 0}, i->normale);
	return (1);
}

t_error		square_parse(t_scene *scene, char **splited_values)
{
	t_list		*new_list;
	t_square	*new_square;
	t_object	*new_object;

	if ((new_square = (t_square *)malloc(sizeof(t_square))) == NULL)
		return (err_malloc);
	if ((new_object = (t_object *)malloc(sizeof(t_object))) == NULL)
		return (clear_obj(new_square, NULL) + err_malloc);
	if (parse_vectr(splited_values[1], &new_square->point) != ok ||
		parse_vectr(splited_values[2], &new_square->normale) != ok ||
		parse_doubl(splited_values[3], &new_square->sidesize) != ok ||
		parse_color(splited_values[4], &new_object->color) != ok ||
		is_normalized(new_square->normale) != ok || new_square->sidesize <= 0)
		return (clear_obj(new_square, new_object) + err_invalid_file);
	if (is_same((t_vectr){0, 0, 0}, new_square->normale))
		new_square->normale = (t_vectr){0, 0, -1};
	new_object->data = new_square;
	new_object->intersect = &intersect;
	if ((new_list = ft_lstnew(new_object)) == NULL)
		return (clear_obj(new_square, new_object) + err_malloc);
	ft_lstadd_back(&(scene->objects), new_list);
	return (ok);
}
