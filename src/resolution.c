/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolution.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:22:45 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 17:32:19 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"

t_error		resolution_parse(t_scene *scene, char **splited_values)
{
	int		w;
	int		h;

	if (splited_values[1] == NULL || scene->has_res++ != 0)
		return (err_invalid_file);
	w = dummy_atoi(splited_values[1]);
	if (splited_values[2] == NULL)
		h = w;
	else
		h = dummy_atoi(splited_values[2]);
	scene->window.w = w;
	scene->window.h = h;
	if (h <= 0 || w <= 0)
		return (err_invalid_file);
	return (ok);
}
