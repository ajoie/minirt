/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 17:10:47 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/14 10:33:24 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"

t_error		is_file_name_valid(const char *path)
{
	int		i;
	char	*extension;

	extension = ".rt";
	i = ft_strlen(path);
	if ((i -= ft_strlen(extension)) <= 0)
		return (err_bad_file_name);
	if (ft_strncmp(&path[i], extension, -1) != 0)
		return (err_bad_file_name);
	return (ok);
}

t_error		parse_color(const char *str, t_color *color)
{
	char	**split;
	float	tmp;

	if (str == NULL)
		return (err_invalid_file);
	tmp = 0;
	split = ft_split(str, ",");
	if (split[0] && (tmp = dummy_atoi(split[0])) < 0)
		return (free_split(split) + err_invalid_file);
	color->r = tmp / 255;
	if (split[1] && (tmp = dummy_atoi(split[1])) < 0)
		return (free_split(split) + err_invalid_file);
	color->g = tmp / 255;
	if (split[2] && (tmp = dummy_atoi(split[2])) < 0)
		return (free_split(split) + err_invalid_file);
	if (split[3] != NULL)
		return (free_split(split) + err_invalid_file);
	color->b = tmp / 255;
	return (free_split(split) + ok);
}

t_error		parse_vectr(const char *str, t_vectr *vectr)
{
	char	**split;

	if (str == NULL)
		return (err_invalid_file);
	split = ft_split(str, ",");
	if (split[0] && parse_doubl(split[0], &vectr->i) != ok)
		return (free_split(split) + err_invalid_file);
	if (split[1] && parse_doubl(split[1], &vectr->j) != ok)
		return (free_split(split) + err_invalid_file);
	if (split[2] && parse_doubl(split[2], &vectr->k) != ok)
		return (free_split(split) + err_invalid_file);
	if (split[3] != NULL)
		return (free_split(split) + err_invalid_file);
	return (free_split(split) + ok);
}

int			free_split(char **rr)
{
	int		i;

	i = 0;
	while (rr[i] != NULL)
		free(rr[i++]);
	free(rr);
	return (0);
}
