/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triangle.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:23:17 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/06 14:03:41 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"

static int	solve(t_intersection *i, t_triangle *tr, t_vectr v[], double d[])
{
	v[0] = m(tr->b, tr->a);
	v[1] = m(tr->c, tr->a);
	v[2] = vxv(i->ray.direction, v[1]);
	d[0] = dot(v[0], v[2]);
	if (d[0] < 1e-8 && d[0] > -1e-8)
		return (0);
	v[3] = m(i->ray.origin, tr->a);
	d[1] = dot(v[3], v[2]) / d[0];
	if (d[1] < 0 || d[1] > 1)
		return (0);
	v[4] = vxv(v[3], v[0]);
	d[2] = dot(i->ray.direction, v[4]) / d[0];
	if (d[2] < 0 || d[1] + d[2] > 1)
		return (0);
	return (1);
}

static int	intersect(void *vi)
{
	t_intersection	*i;
	t_triangle		*tr;
	t_vectr			varr[5];
	double			darr[3];

	i = vi;
	tr = (t_triangle *)i->obj->data;
	if (solve(i, tr, varr, darr) == 0)
		return (0);
	i->t = dot(varr[1], varr[4]) / darr[0];
	i->point = p(i->ray.origin, x(i->ray.direction, i->t));
	i->normale = norm(vxv(varr[0], varr[1]));
	if (dot(i->ray.direction, i->normale) > 0)
		i->normale = m((t_vectr){0, 0, 0}, i->normale);
	return (1);
}

t_error		triangle_parse(t_scene *scene, char **splited_values)
{
	t_list		*new_list;
	t_triangle	*new_triangle;
	t_object	*new_object;

	if ((new_triangle = (t_triangle *)malloc(sizeof(t_triangle))) == NULL)
		return (err_malloc);
	if ((new_object = (t_object *)malloc(sizeof(t_object))) == NULL)
		return (clear_obj(new_triangle, NULL) + err_malloc);
	if (parse_vectr(splited_values[1], &new_triangle->a) != ok ||
		parse_vectr(splited_values[2], &new_triangle->b) != ok ||
		parse_vectr(splited_values[3], &new_triangle->c) != ok ||
		parse_color(splited_values[4], &new_object->color) != ok)
		return (clear_obj(new_triangle, new_object) + err_invalid_file);
	new_object->data = new_triangle;
	new_object->intersect = &intersect;
	if ((new_list = ft_lstnew(new_object)) == NULL)
		return (clear_obj(new_triangle, new_object) + err_malloc);
	ft_lstadd_back(&(scene->objects), new_list);
	return (ok);
}
