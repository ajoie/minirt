/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 16:13:58 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/14 12:37:03 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"

t_error		parse_line(t_scene *scene, char **sv)
{
	if (sv[0] == NULL || sv[0][0] == '#')
		return (ok);
	else if (ft_strncmp("R", sv[0], -1) == 0)
		return (resolution_parse(scene, sv));
	else if (ft_strncmp("A", sv[0], -1) == 0)
		return (ambient_parse(scene, sv));
	else if (ft_strncmp("c", sv[0], -1) == 0)
		return (camera_parse(scene, sv));
	else if (ft_strncmp("l", sv[0], -1) == 0)
		return (light_parse(scene, sv));
	else if (ft_strncmp("sp", sv[0], -1) == 0)
		return (sphere_parse(scene, sv));
	else if (ft_strncmp("pl", sv[0], -1) == 0)
		return (plane_parse(scene, sv));
	else if (ft_strncmp("sq", sv[0], -1) == 0)
		return (square_parse(scene, sv));
	else if (ft_strncmp("cy", sv[0], -1) == 0)
		return (cylinder_parse(scene, sv));
	else if (ft_strncmp("tr", sv[0], -1) == 0)
		return (triangle_parse(scene, sv));
	else if (ft_strncmp("AA", sv[0], -1) == 0)
		return (antialiasing_parse(scene, sv));
	return (err_invalid_file);
}

t_error		parse_file(t_scene *scene, int fd)
{
	int		c;
	int		exit;
	char	*line;
	char	**sv;
	t_error	r;

	exit = 0;
	r = ok;
	while (!exit && (c = get_next_line(fd, &line)) != -1)
	{
		if (r == ok)
		{
			sv = ft_split(line, "\t ");
			r = parse_line(scene, sv);
			free_split(sv);
		}
		free(line);
		if (c <= 0)
			exit = 1;
	}
	if (c == -1)
		return (err_invalid_file);
	if (r != ok)
		return (r);
	return (ok);
}

t_error		parser(t_scene *scene, int argc, char *argv[])
{
	t_error		err;

	if (argc < 2)
		return (err_bad_number_of_args);
	if (argc == 3 && ft_strncmp("--save", argv[2], -1) == 0)
		scene->save = 1;
	else if (argc == 3)
		return (err_bad_argument);
	if ((err = is_file_name_valid(argv[1])) != ok)
		return (err);
	if ((err = parse_file(scene, open(argv[1], O_RDONLY))) != ok)
		return (err);
	if (ft_lstsize(scene->cams) < 1)
		return (err_invalid_file);
	return (ok);
}
