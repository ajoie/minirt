/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 13:24:25 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 19:12:22 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"
#include "parser.h"

static void		copy(t_intersection *i, t_intersection li)
{
	i->normale = li.normale;
	i->point = li.point;
	i->t = li.t;
	i->obj = li.obj;
	i->color = li.obj->color;
}

int				clear_obj(void *a, void *b)
{
	if (a != NULL)
		free(a);
	if (b != NULL)
		free(b);
	return (0);
}

int				get_nearest_object(t_scene *scene, t_intersection *i)
{
	t_list			*ptr_o;
	t_intersection	li;
	int				ret;

	ret = 0;
	i->t = 1e20;
	li.ray = i->ray;
	ptr_o = scene->objects;
	while (ptr_o != NULL)
	{
		li.obj = ptr_o->content;
		if (li.obj->intersect(&li))
		{
			if (li.t < i->t && 1e-9 < li.t)
			{
				copy(i, li);
				ret = 1;
			}
		}
		ptr_o = ptr_o->next;
	}
	return (ret);
}
