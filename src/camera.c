/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:22:54 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 19:29:58 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"
#include <math.h>

t_vectr			camera_lookat(t_vectr v, t_vectr dir)
{
	double		matrix[3][3];
	t_vectr		right;
	t_vectr		up;
	t_vectr		res;

	up = (t_vectr){0.0, 1.0, 0.0};
	right = vxv(up, dir);
	if (length(right) < 1e-8)
		right = (t_vectr){1.0, 0.0, 0.0};
	right = norm(right);
	up = vxv(dir, right);
	matrix[0][0] = right.i;
	matrix[0][1] = right.j;
	matrix[0][2] = right.k;
	matrix[1][0] = up.i;
	matrix[1][1] = up.j;
	matrix[1][2] = up.k;
	matrix[2][0] = dir.i;
	matrix[2][1] = dir.j;
	matrix[2][2] = dir.k;
	res.i = matrix[0][0] * v.i + matrix[1][0] * v.j + matrix[2][0] * v.k;
	res.j = matrix[0][1] * v.i + matrix[1][1] * v.j + matrix[2][1] * v.k;
	res.k = matrix[0][2] * v.i + matrix[1][2] * v.j + matrix[2][2] * v.k;
	return (norm(res));
}

t_error			camera_parse(t_scene *scene, char **splited_values)
{
	t_list		*new_list;
	t_camera	*new_camera;

	if ((new_camera = (t_camera *)malloc(sizeof(t_camera))) == NULL)
		return (err_malloc);
	if (parse_vectr(splited_values[1], &new_camera->point) != ok ||
		parse_vectr(splited_values[2], &new_camera->normale) != ok ||
		parse_doubl(splited_values[3], &new_camera->angle) != ok ||
		is_normalized(new_camera->normale) != ok)
		return (clear_obj(new_camera, NULL) + err_invalid_file);
	if (!(0 <= new_camera->angle && new_camera->angle <= 180))
		return (clear_obj(new_camera, NULL) + err_invalid_file);
	if (is_same((t_vectr){0, 0, 0}, new_camera->normale))
		new_camera->normale = (t_vectr){0, 0, 1};
	new_camera->fov = new_camera->angle * M_PI / 180;
	if ((new_list = ft_lstnew(new_camera)) == NULL)
		return (clear_obj(new_camera, NULL) + err_malloc);
	ft_lstadd_back(&(scene->cams), new_list);
	return (ok);
}
