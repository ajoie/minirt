/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processor.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 11:19:54 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/08 00:14:19 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"

void		get_ray(t_intersection *i, t_scene *s, double x, double y)
{
	t_vectr	dir;
	t_vectr	pnt;
	t_vectr	cam_dir;

	pnt = s->cam->point;
	cam_dir = s->cam->normale;
	dir.i = x - s->window.w / 2;
	dir.j = y - s->window.h / 2;
	dir.k = s->window.w / (2 * tan(s->cam->fov / 2));
	dir = norm(dir);
	dir = camera_lookat(dir, norm(cam_dir));
	dir = norm(dir);
	i->ray = (t_ray){pnt, dir};
}

void		get_pixel_color(t_scene *s, t_intersection *i, int x, int y)
{
	i->light = (t_color){0, 0, 0};
	i->color = (t_color){0, 0, 0};
	get_ray(i, s, x, y);
	i->obj = NULL;
	if (get_nearest_object(s, i))
	{
		light(s, i);
		color_apply(i);
	}
}

t_error		draw(t_scene *s)
{
	t_intersection	i;
	int				x;
	int				y;

	y = 0;
	while (y < s->window.h)
	{
		x = 0;
		while (x < s->window.w)
		{
			if (s->aa > 1)
				get_pixel_color_aa(s, &i, x, y);
			else
				get_pixel_color(s, &i, x, y);
			s->cam->raw_img[s->window.w * y + x] = color_to_int(i.color);
			++x;
		}
		++y;
	}
	return (ok);
}

t_error		calc_all_images(t_scene *s)
{
	t_list		*ptr;
	t_camera	*cam;

	ptr = s->cams;
	while (ptr != NULL)
	{
		cam = ptr->content;
		if (!(cam->raw_img = (int *)malloc(s->window.w * s->window.h * 32)))
			return (err_malloc);
		s->cam = cam;
		ptr = ptr->next;
		draw(s);
	}
	return (ok);
}

t_error		processor(t_scene *s)
{
	if (!s->save)
		mlx_rt_init(s);
	if (calc_all_images(s) != ok)
		return (err_malloc);
	if (!s->save)
		mlx_rt_window_init(s);
	if (s->save)
		bmp_gen_images(s);
	return (ok);
}
