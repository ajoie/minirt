/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_basic.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 20:05:09 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 18:05:26 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

t_vectr		p(t_vectr a, t_vectr b)
{
	t_vectr res;

	res.i = a.i + b.i;
	res.j = a.j + b.j;
	res.k = a.k + b.k;
	return (res);
}

t_vectr		m(t_vectr a, t_vectr b)
{
	t_vectr res;

	res.i = a.i - b.i;
	res.j = a.j - b.j;
	res.k = a.k - b.k;
	return (res);
}

t_vectr		x(t_vectr a, double b)
{
	t_vectr res;

	res.i = a.i * b;
	res.j = a.j * b;
	res.k = a.k * b;
	return (res);
}

t_vectr		d(t_vectr a, double b)
{
	t_vectr res;

	res.i = a.i / b;
	res.j = a.j / b;
	res.k = a.k / b;
	return (res);
}

t_error		is_normalized(t_vectr a)
{
	if (fabs(a.i) > 1 + 1e-8)
		return (err_invalid_file);
	if (fabs(a.j) > 1 + 1e-8)
		return (err_invalid_file);
	if (fabs(a.k) > 1 + 1e-8)
		return (err_invalid_file);
	return (ok);
}
