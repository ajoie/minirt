/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 11:12:38 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/08 00:22:53 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"
#include <fcntl.h>

void	bmp_gen_images(t_scene *s)
{
	t_list		*ptr;
	t_camera	*cam;
	char		*name;
	char		*itoa;
	int			i;

	i = 0;
	ptr = s->cams;
	while (ptr != NULL)
	{
		cam = ptr->content;
		itoa = ft_itoa(i);
		name = ft_strjoin(itoa, "_cam_image.bmp");
		bmp_gen((unsigned char *)cam->raw_img, s->window.h, s->window.w, name);
		free(itoa);
		free(name);
		ptr = ptr->next;
		++i;
	}
}

void	bmp_gen(unsigned char *image, int height, int width, char *path)
{
	unsigned char	padding[3];
	int				width_in_bytes;
	int				padding_size;
	int				fd;
	int				i;

	width_in_bytes = width * 4;
	padding_size = (4 - (width_in_bytes) % 4) % 4;
	ft_bzero(padding, sizeof(unsigned char) * 3);
	fd = open(path, O_WRONLY | O_CREAT, 0644);
	i = (width_in_bytes) + padding_size;
	bmp_file_header(fd, height, i);
	bmp_info_header(fd, height, width);
	i = 0;
	while (i < height)
	{
		write(fd, image + (i * width_in_bytes), 4 * width);
		write(fd, padding, 1 * padding_size);
		++i;
	}
	close(fd);
}

void	bmp_file_header(int fd, int height, int stride)
{
	unsigned char	head[14];
	int				size;

	ft_bzero(head, sizeof(unsigned char) * 14);
	size = 14 + 40 + (stride * height);
	head[0] = (unsigned char)('B');
	head[1] = (unsigned char)('M');
	head[2] = (unsigned char)(size);
	head[3] = (unsigned char)(size >> 8);
	head[4] = (unsigned char)(size >> 16);
	head[5] = (unsigned char)(size >> 24);
	head[10] = (unsigned char)(14 + 40);
	write(fd, head, 1 * 14);
}

void	bmp_info_header(int fd, int height, int width)
{
	unsigned char	head[40];

	ft_bzero(head, sizeof(unsigned char) * 40);
	head[0] = (unsigned char)(40);
	head[4] = (unsigned char)(width);
	head[5] = (unsigned char)(width >> 8);
	head[6] = (unsigned char)(width >> 16);
	head[7] = (unsigned char)(width >> 24);
	head[8] = (unsigned char)(height);
	head[9] = (unsigned char)(height >> 8);
	head[10] = (unsigned char)(height >> 16);
	head[11] = (unsigned char)(height >> 24);
	head[12] = (unsigned char)(1);
	head[14] = (unsigned char)(4 * 8);
	write(fd, head, 1 * 40);
}
