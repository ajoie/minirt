/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/02 16:51:18 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/06 14:17:50 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "parser.h"
#include "processor.h"

int		main(int argc, char *argv[])
{
	t_scene scene;
	t_error err;

	if (scene_init(&scene) != ok)
		return (err_malloc);
	if ((err = parser(&scene, argc, argv)) == ok)
		processor(&scene);
	else
	{
		ft_putstr_fd("ERROR.\n", 0);
		ft_putstr_fd(error_to_string(err), 0);
		scene.parse_err = 1;
	}
	scene_free(&scene);
	return (err);
}
