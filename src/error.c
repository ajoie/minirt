/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 18:01:08 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 18:31:43 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "error.h"

char	*error_to_string(t_error e)
{
	if (e == ok)
		return ("no error here)\n");
	if (e == err_bad_number_of_args)
		return ("Bad number of arguments.\n");
	if (e == err_bad_file_name)
		return ("Bad file name.\n");
	if (e == err_invalid_file)
		return ("Invalid file.\n");
	if (e == err_malloc)
		return ("Can't malloc.\n");
	if (e == err_bad_argument)
		return ("Bad argument.\n");
	return ("Unknown error(\n");
}
