/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 10:13:01 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/06 14:14:00 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"
#include "keycodes.h"
#include "mlx.h"

int		hook_image_put(t_scene *s)
{
	t_list		*ptr;
	t_camera	*cam;

	ptr = ft_lstnth(s->cams, s->active_cam_id);
	cam = ptr->content;
	mlx_put_image_to_window(s->window.mlx_ptr, s->window.win_ptr,
							cam->mlx_img.img, 0, 0);
	return (1);
}

int		hook_close_and_free(t_scene *s)
{
	mlx_clear_window(s->window.mlx_ptr, s->window.win_ptr);
	mlx_destroy_window(s->window.mlx_ptr, s->window.win_ptr);
	free_mlx_cams(s);
	scene_free(s);
	exit(0);
	return (1);
}

void	hook_cam_next(t_scene *s)
{
	if (ft_lstnth(s->cams, s->active_cam_id + 1) != NULL)
		s->active_cam_id += 1;
}

void	hook_cam_prev(t_scene *s)
{
	if (ft_lstnth(s->cams, s->active_cam_id - 1) != NULL)
		s->active_cam_id -= 1;
}

int		hook_keypress(int keycode, t_scene *s)
{
	if (keycode == KEY_ESC || keycode == KEY_Q)
		hook_close_and_free(s);
	else if (keycode == KEY_N)
		hook_cam_next(s);
	else if (keycode == KEY_P)
		hook_cam_prev(s);
	return (1);
}
