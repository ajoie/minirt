/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:23:15 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 22:16:22 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"
#include <math.h>

static int		cut(t_ray ray, t_cylinder *cy, double t)
{
	double		c;
	t_vectr		pnt;

	pnt = p(ray.origin, x(ray.direction, t));
	c = sqrt(pow(cy->height / 2, 2) + pow(cy->diameter / 2, 2));
	if (fabs(length(m(pnt, cy->point))) < c)
		return (1);
	return (0);
}

int				solve(t_intersection *i, t_cylinder *cy, double *darr)
{
	double	d;

	if ((darr[3] = pow(darr[1], 2) - (4 * darr[0] * darr[2])) < 0)
		return (0);
	if ((darr[5] = (-darr[1] + sqrt(darr[3])) / (2 * darr[0])) < 0)
		return (0);
	if ((darr[4] = (-darr[1] - sqrt(darr[3])) / (2 * darr[0])) > 0 &&
		cut(i->ray, cy, darr[4]))
		i->t = darr[4];
	else if (cut(i->ray, cy, darr[5]))
		i->t = darr[5];
	else
		return (0);
	i->point = p(i->ray.origin, x(i->ray.direction, i->t));
	d = pow(length(m(i->point, cy->point)), 2) - pow(cy->diameter / 2, 2);
	if (fabs(d) < 1e-4)
		i->normale = norm(m(cy->point, i->point));
	else if (dot(cy->normale, m(cy->point, i->point)) < 0)
		i->normale = norm(m(p(cy->point, x(cy->normale, sqrt(d))), i->point));
	else
		i->normale = norm(m(m(cy->point, x(cy->normale, sqrt(d))), i->point));
	if (i->t == darr[4])
		i->normale = m((t_vectr){0, 0, 0}, i->normale);
	return (1);
}

static int		intersect(void *vi)
{
	t_intersection	*i;
	t_cylinder		*cy;
	t_vectr			varr[2];
	double			darr[6];

	i = vi;
	cy = (t_cylinder *)i->obj->data;
	varr[0] = vxv(i->ray.direction, cy->normale);
	varr[1] = vxv(m(i->ray.origin, cy->point), cy->normale);
	darr[0] = dot(varr[0], varr[0]);
	darr[1] = 2 * dot(varr[0], varr[1]);
	darr[2] = dot(varr[1], varr[1]) - (pow(cy->diameter / 2, 2)
		* dot(cy->normale, cy->normale));
	return (solve(i, cy, darr));
}

t_error			cylinder_parse(t_scene *scene, char **splited_values)
{
	t_list		*new_list;
	t_cylinder	*new_cylinder;
	t_object	*new_object;

	if ((new_cylinder = (t_cylinder *)malloc(sizeof(t_cylinder))) == NULL)
		return (err_malloc);
	if ((new_object = (t_object *)malloc(sizeof(t_object))) == NULL)
		return (clear_obj(new_cylinder, NULL) + err_malloc);
	if (parse_vectr(splited_values[1], &new_cylinder->point) != ok ||
		parse_vectr(splited_values[2], &new_cylinder->normale) != ok ||
		parse_doubl(splited_values[3], &new_cylinder->diameter) != ok ||
		parse_doubl(splited_values[4], &new_cylinder->height) != ok ||
		parse_color(splited_values[5], &new_object->color) != ok ||
		is_normalized(new_cylinder->normale) != ok ||
		new_cylinder->height <= 0 || new_cylinder->diameter <= 0)
		return (clear_obj(new_cylinder, new_object) + err_invalid_file);
	if (is_same((t_vectr){0, 0, 0}, new_cylinder->normale))
		new_cylinder->normale = (t_vectr){0, 1, 0};
	new_object->data = new_cylinder;
	new_object->intersect = &intersect;
	if ((new_list = ft_lstnew(new_object)) == NULL)
		return (clear_obj(new_cylinder, new_object) + err_malloc);
	ft_lstadd_back(&(scene->objects), new_list);
	return (ok);
}
