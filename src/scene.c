/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 16:08:52 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/06 14:19:32 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"
#include "processor.h"

void		free_mlx_cams(t_scene *s)
{
	t_list		*ptr;
	t_camera	*cam;

	ptr = s->cams;
	while (ptr != NULL)
	{
		cam = ptr->content;
		if (cam->mlx_img.img)
			mlx_destroy_image(s->window.mlx_ptr, cam->mlx_img.img);
		ptr = ptr->next;
	}
}

static void	free_cams(void *o)
{
	t_camera *obj;

	obj = o;
	free(obj->raw_img);
	free(o);
}

static void	free_objects(void *o)
{
	t_object *obj;

	obj = o;
	free(obj->data);
	free(o);
}

void		scene_free(t_scene *s)
{
	if (s->parse_err)
		ft_lstclear(&s->cams, &free);
	else
		ft_lstclear(&s->cams, &free_cams);
	ft_lstclear(&s->lights, &free);
	ft_lstclear(&s->objects, &free_objects);
}

t_error		scene_init(t_scene *s)
{
	s->objects = NULL;
	s->cams = NULL;
	s->lights = NULL;
	s->active_cam_id = 0;
	s->aa = 1;
	s->save = 0;
	s->has_aa = 0;
	s->has_amb = 0;
	s->has_res = 0;
	s->parse_err = 0;
	return (ok);
}
