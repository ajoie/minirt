/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plane.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 15:23:12 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/07 20:30:59 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "minirt.h"

static int		intersect(void *vi)
{
	t_intersection	*i;
	t_plane			*pl;
	double			c;
	double			z;

	i = vi;
	pl = (t_plane *)i->obj->data;
	z = dot(i->ray.direction, pl->normale);
	if (z < 1e-8 && z > -1e-8)
		return (0);
	c = -dot(m(i->ray.origin, pl->point), pl->normale);
	i->t = c / z;
	if (i->t < 0)
		return (0);
	i->point = p(i->ray.origin, x(i->ray.direction, i->t));
	i->normale = norm(pl->normale);
	if (dot(i->ray.direction, pl->normale) > 0)
		i->normale = m((t_vectr){0, 0, 0}, i->normale);
	return (1);
}

t_error			plane_parse(t_scene *scene, char **splited_values)
{
	t_list		*new_list;
	t_plane		*new_plane;
	t_object	*new_object;

	if ((new_plane = (t_plane *)malloc(sizeof(t_plane))) == NULL)
		return (err_malloc);
	if ((new_object = (t_object *)malloc(sizeof(t_object))) == NULL)
		return (clear_obj(new_plane, NULL) + err_malloc);
	if (parse_vectr(splited_values[1], &new_plane->point) != ok ||
		parse_vectr(splited_values[2], &new_plane->normale) != ok ||
		parse_color(splited_values[3], &new_object->color) != ok ||
		is_normalized(new_plane->normale) != ok)
		return (clear_obj(new_plane, new_object) + err_invalid_file);
	if (is_same((t_vectr){0, 0, 0}, new_plane->normale))
		new_plane->normale = (t_vectr){0, 1, 0};
	new_object->data = new_plane;
	new_object->intersect = &intersect;
	if ((new_list = ft_lstnew(new_object)) == NULL)
		return (clear_obj(new_plane, new_object) + err_malloc);
	ft_lstadd_back(&(scene->objects), new_list);
	return (ok);
}
