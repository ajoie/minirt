/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/17 17:42:48 by ajoie             #+#    #+#             */
/*   Updated: 2021/01/04 18:49:21 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	set_next_line(char *n, char **line, char **str)
{
	*n++ = 0;
	if (!(*line = gnl_strdup(*str)))
		return (-1);
	n = gnl_strdup(n);
	free(*str);
	*str = n;
	return (1);
}

int			get_next_line(int fd, char **line)
{
	static char	*str;
	char		*tmp;
	char		*buf;
	ssize_t		pos;

	if (BUFFER_SIZE < 1 || line == NULL)
		return (-1);
	if ((tmp = gnl_strchr(str, '\n')))
		return (set_next_line(tmp, line, &str));
	if (!(buf = (char *)malloc(BUFFER_SIZE + 1)))
		return (-1);
	if ((pos = read(fd, buf, BUFFER_SIZE)) >= 0)
		buf[pos] = 0;
	if (pos <= 0)
	{
		*line = gnl_strdup(str);
		free(str);
		str = NULL;
		free(buf);
		if (!line)
			return (-1);
		return (pos);
	}
	str = gnl_strjoin(str, buf);
	return (get_next_line(fd, line));
}
