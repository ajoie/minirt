/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils_bonus.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/20 13:39:00 by ajoie             #+#    #+#             */
/*   Updated: 2021/01/04 18:47:39 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

size_t	gnl_strlen(const char *s)
{
	size_t len;

	if (!s)
		return (0);
	len = 0;
	while (s[len] != 0)
		++len;
	return (len);
}

char	*gnl_strjoin(char *s1, char *s2)
{
	size_t	len;
	char	*str;
	char	*s1start;
	char	*s2start;

	s1start = s1;
	s2start = s2;
	if (!s1 && !s2)
		return (NULL);
	len = gnl_strlen(s1) + gnl_strlen(s2);
	str = (char *)malloc((len + 1) * sizeof(char));
	if (!str)
		return (NULL);
	while (s1 && *s1)
		*str++ = *s1++;
	while (s2 && *s2)
		*str++ = *s2++;
	*str = 0;
	free(s1start);
	free(s2start);
	return (str - len);
}

char	*gnl_strchr(const char *s, int c)
{
	if (!s)
		return (NULL);
	while (*s && *s != c)
		++s;
	if (*s == c)
		return ((char*)s);
	return (NULL);
}

char	*gnl_strdup(const char *s1)
{
	int		len;
	char	*p;

	len = gnl_strlen(s1);
	p = (char *)malloc((len + 1) * sizeof(char));
	if (!p)
		return (NULL);
	if (!s1)
	{
		p[0] = 0;
		return (p);
	}
	len = 0;
	while (s1[len] != 0)
	{
		p[len] = s1[len];
		++len;
	}
	p[len] = 0;
	return (p);
}
