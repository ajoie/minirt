/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 16:43:55 by ajoie             #+#    #+#             */
/*   Updated: 2021/01/22 19:08:30 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char *us1;
	unsigned char *us2;

	us1 = (unsigned char*)s1;
	us2 = (unsigned char*)s2;
	if (!s1 || !s2)
		return (-1);
	while (n-- && (*us1 != 0 || *us2 != 0))
	{
		if (*us1 != *us2)
			return (*us1 - *us2);
		++us1;
		++us2;
	}
	return (0);
}
