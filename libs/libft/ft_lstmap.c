/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/07 14:20:18 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/09 19:56:54 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list *start;
	t_list *ptr;

	if (!lst)
		return (NULL);
	start = ft_lstnew((*f)(lst->content));
	if (!start)
		return (NULL);
	ptr = start;
	lst = lst->next;
	while (lst != NULL)
	{
		ptr->next = ft_lstnew((*f)(lst->content));
		if (!ptr->next)
		{
			ft_lstclear(&start, del);
			return (NULL);
		}
		ptr = ptr->next;
		lst = lst->next;
	}
	return (start);
}
