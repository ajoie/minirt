/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/29 16:21:42 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 15:19:03 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *restrict dst, const char *restrict src, size_t dstsize)
{
	const char *srcstart;

	srcstart = src;
	if (!src || !dst)
		return (0);
	if (dstsize == 0)
		return (ft_strlen(srcstart));
	while (--dstsize && *src)
		*dst++ = *src++;
	*dst = 0;
	return (ft_strlen(srcstart));
}
