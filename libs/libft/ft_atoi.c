/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/29 19:28:25 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 14:41:25 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_isspace(int c)
{
	return (c == '\t' || c == '\n' || c == '\v' ||
			c == '\f' || c == '\r' || c == ' ');
}

int		ft_atoi(const char *str)
{
	unsigned long int	res;
	int					k;

	res = 0;
	k = 1;
	while (ft_isspace(*str))
		++str;
	if (*str == '-' || *str == '+')
		if (*str++ == '-')
			k = -1;
	while (*str != 0 && ft_isdigit(*str))
	{
		res *= 10;
		res += (*str++) - '0';
	}
	if (res > 9223372036854775807 && k == 1)
		return (-1);
	if (res - 1 > 9223372036854775807 && k == -1)
		return (0);
	return (k * res);
}
