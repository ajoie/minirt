/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/31 14:17:56 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 14:47:16 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *restrict dst, const void *restrict src, int c,
		size_t n)
{
	char *udst;
	char *usrc;

	c = (char)c;
	udst = (char*)dst;
	usrc = (char*)src;
	while (n--)
	{
		dst++;
		if ((*udst++ = *usrc++) == c)
			return (dst);
	}
	return (NULL);
}
