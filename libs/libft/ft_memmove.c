/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/31 18:40:23 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 15:00:58 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char *udst;
	char *usrc;

	if (dst == NULL && src == NULL)
		return (dst);
	udst = (char*)dst;
	usrc = (char*)src;
	if (src > dst)
		while (len--)
			*udst++ = *usrc++;
	else if (src < dst)
		while (len--)
			*(udst + len) = *(usrc + len);
	return (dst);
}
