/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 14:41:41 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 15:09:24 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t len;

	len = 0;
	while (*(s + len) != 0)
		++len;
	while (*(s + len) != c && len)
		--len;
	if (*(s + len) == c)
		return ((char*)(s + len));
	return (NULL);
}
