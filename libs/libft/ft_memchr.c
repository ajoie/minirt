/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/31 13:24:14 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/12 15:25:33 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char *ucs;

	ucs = s;
	while (n != 0)
	{
		if (*ucs == (unsigned char)c)
			return ((void *)ucs);
		--n;
		++ucs;
	}
	return (NULL);
}
