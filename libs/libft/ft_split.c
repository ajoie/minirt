/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 13:45:42 by ajoie             #+#    #+#             */
/*   Updated: 2021/03/04 18:24:58 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_words(char const *s, const char *sep)
{
	int		words;
	int		prev;

	words = 0;
	prev = sep[0];
	while (*s)
	{
		if (!ft_strchr(sep, *s) && ft_strchr(sep, prev))
			++words;
		prev = *s++;
	}
	return (words);
}

char		*ft_nextword(char **s, const char *sep)
{
	char	*cs;
	char	*word;
	size_t	len;

	len = 0;
	while (ft_strchr(sep, **s))
		++*s;
	cs = *s;
	while (cs[len] && !ft_strchr(sep, cs[len]))
		++len;
	word = (char *)malloc((len + 1) * sizeof(char));
	if (!word)
		return (NULL);
	ft_memcpy(word, *s, len);
	word[len] = 0;
	while (!ft_strchr(sep, **s) && **s != 0)
		++*s;
	return (word);
}

static void	ft_freein(char ***rr, size_t l)
{
	while (l)
		free(&(**rr[l--]));
	free(**rr);
}

char		**ft_split(char const *s, const char *sep)
{
	char	*cs;
	size_t	words;
	size_t	pos;
	char	**split;

	if (!s)
		return (NULL);
	pos = 0;
	cs = (char *)s;
	words = ft_words(s, sep);
	split = (char **)malloc((words + 1) * sizeof(char *));
	if (!split)
		return (NULL);
	while (words--)
	{
		if (!(split[pos++] = ft_nextword(&cs, sep)))
		{
			ft_freein(&split, pos - 1);
			return (NULL);
		}
	}
	split[pos] = NULL;
	return (split);
}
