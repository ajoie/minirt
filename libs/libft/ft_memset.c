/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/31 12:17:13 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 15:01:58 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	char			*cb;
	unsigned int	cc;

	cc = (unsigned int)c;
	cb = (char*)b;
	while (len--)
		*cb++ = cc;
	return (b);
}
