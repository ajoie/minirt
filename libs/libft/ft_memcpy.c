/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/31 14:22:59 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 14:59:06 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *restrict dst, const void *restrict src, size_t n)
{
	char *udst;
	char *usrc;

	if (dst == NULL && src == NULL)
		return (dst);
	udst = (char*)dst;
	usrc = (char*)src;
	while (n--)
		*udst++ = *usrc++;
	return (dst);
}
