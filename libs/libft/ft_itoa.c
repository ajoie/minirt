/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 15:31:21 by ajoie             #+#    #+#             */
/*   Updated: 2020/12/29 16:56:10 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_ilen(int n)
{
	size_t len;

	len = 1;
	if (n < 0)
		++len;
	while ((n /= 10) != 0)
		++len;
	return (len);
}

char	*ft_itoa(int n)
{
	size_t			len;
	char			*a;
	unsigned int	un;

	len = ft_ilen(n);
	a = (char *)malloc((len + 1) * sizeof(char));
	if (!a)
		return (NULL);
	if (n < 0)
		un = n * -1;
	else
		un = n;
	a[len] = 0;
	a[0] = '0';
	while (un != 0)
	{
		a[--len] = '0' + un % 10;
		un /= 10;
	}
	if (n < 0)
		a[0] = '-';
	return (a);
}
