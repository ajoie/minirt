/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/06 14:42:56 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/07 11:21:15 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **lst, t_list *new)
{
	if (new && *lst)
	{
		new->next = NULL;
		ft_lstlast(*lst)->next = new;
	}
	if (!*lst)
		*lst = new;
}
