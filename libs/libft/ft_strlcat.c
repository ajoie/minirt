/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/29 17:44:44 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/12 14:23:54 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *restrict dst, const char *restrict src, size_t dstsize)
{
	size_t di;
	size_t si;

	di = 0;
	si = 0;
	while (dst[di] != 0 && di < dstsize)
		++di;
	while (di + si + 1 < dstsize && src[si] != 0)
	{
		dst[di + si] = src[si];
		++si;
	}
	if (si != 0)
		dst[di + si] = 0;
	return (di + ft_strlen(src));
}
