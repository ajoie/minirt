/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:27:08 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 16:32:01 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	int		len;
	char	*p;

	len = 0;
	while (s1[len] != 0)
		len++;
	p = (char*)malloc((len + 1) * sizeof(char));
	if (!p)
		return (NULL);
	len = 0;
	while (s1[len] != 0)
	{
		p[len] = s1[len];
		++len;
	}
	p[len] = 0;
	return (p);
}
