/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 15:33:00 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/04 15:09:14 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t pos;
	size_t j;

	if (*needle == 0)
		return ((char *)haystack);
	pos = 0;
	while (pos < len && haystack[pos] != 0)
	{
		j = 0;
		while (haystack[pos + j] == needle[j] && pos + j < len)
			if (needle[++j] == 0)
				return ((char *)(&haystack[pos]));
		pos++;
	}
	return (NULL);
}
