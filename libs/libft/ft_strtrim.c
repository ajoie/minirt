/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 12:22:49 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/07 18:45:09 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_offtop(char const *s1, char const *set)
{
	size_t i;
	size_t j;
	size_t top;

	i = 0;
	top = 0;
	while (s1[i] && i == top)
	{
		j = 0;
		while (set[j] && i == top)
			if (s1[i] == set[j++])
				++top;
		++i;
	}
	return (top);
}

static size_t	ft_offbottom(char const *s1, char const *set)
{
	size_t i;
	size_t j;
	size_t len;
	size_t bottom;

	i = 0;
	bottom = 0;
	i = ft_strlen(s1);
	len = --i;
	while (i && (len - i) == bottom)
	{
		j = 0;
		while (set[j] && (len - i) == bottom)
			if (s1[i] == set[j++])
				++bottom;
		--i;
	}
	return (bottom);
}

char			*ft_strtrim(char const *s1, char const *set)
{
	size_t	top;
	size_t	bot;
	size_t	len;
	char	*str;

	if (!s1 || !set)
		return (NULL);
	len = ft_strlen(s1);
	top = ft_offtop(s1, set);
	if (top != len)
		bot = ft_offbottom(s1, set);
	else
		bot = 0;
	str = (char*)malloc((len - top - bot + 1) * sizeof(char));
	if (!str)
		return (NULL);
	ft_memcpy(str, s1 + top, len - top - bot);
	str[len - top - bot] = 0;
	return (str);
}
