/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ajoie <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/04 00:41:11 by ajoie             #+#    #+#             */
/*   Updated: 2020/11/06 12:13:45 by ajoie            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t	len;
	size_t	i;
	char	*map;

	if (!s)
		return (NULL);
	len = ft_strlen(s);
	map = (char *)malloc((len + 1) * sizeof(char));
	if (!map)
		return (NULL);
	i = 0;
	while (i < len)
	{
		map[i] = (*f)(i, s[i]);
		++i;
	}
	map[len] = 0;
	return (map);
}
