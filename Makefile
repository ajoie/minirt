# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ajoie <ajoie@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/01/08 17:37:26 by ajoie             #+#    #+#              #
#    Updated: 2021/03/14 10:53:08 by ajoie            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

MAKEFLAGS += --no-print-directory
FILES = ambient.c antialiasing_bonus.c bmp.c camera.c color.c cylinder.c\
	double_parser.c error.c hooks.c light.c main.c mlx.c objects.c parser.c\
	parser_tools.c plane.c processor.c resolution.c scene.c sphere.c square.c\
	triangle.c vector_additional.c vector_basic.c
NAME = miniRT
SRCDIR = ./src
OBJDIR = ./obj
SRC = $(addprefix $(SRCDIR)/,$(FILES))
OBJ = $(addprefix $(OBJDIR)/,$(FILES:.c=.o))
CC = gcc
FLAGS = -Wall -Werror -Wextra
INC = -I./inc -I./libs/libft -I./libs/get_next_line -I./libs/minilibx
LIBFT = ./libs/libft/libft.a
GNL = ./libs/get_next_line/get_next_line.a
LIBS = -lm $(LIBFT) $(GNL) $(MLX)
ifeq ($(shell uname), Darwin)
	MLX = ./libmlx.dylib
	LIBS += -lmlx -framework OpenGL -framework AppKit
endif
ifeq ($(shell uname), Linux)
	# https://github.com/ttshivhula/minilibx
	LIBS += -lmlx -lXext -lX11
endif

all: $(NAME)

$(NAME): $(LIBFT) $(GNL) $(MLX) $(OBJDIR) $(OBJ)
	@gcc $(FLAGS) $(INC) -o $(NAME) $(OBJ) $(LIBS)
	@echo "DONE $@"

$(MLX):
	@$(MAKE) all -C ./libs/minilibx
	@cp ./libs/minilibx/libmlx.dylib ./

$(GNL):
	@$(MAKE) bonus -C ./libs/get_next_line

$(LIBFT):
	@$(MAKE) bonus -C ./libs/libft

$(OBJDIR):
	@mkdir $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@$(CC) -c $(FLAGS) $(INC) $< -o $@
	@echo "$< ==> $@"

clean:
	@$(MAKE) clean -C ./libs/libft
	@$(MAKE) clean -C ./libs/get_next_line
	@$(MAKE) clean -C ./libs/minilibx
	@rm -f $(OBJ)
	@rm -rf *.dSYM
	@rm -rf *.bmp
	@echo "CLEAN DONE"

fclean: clean
	@$(MAKE) fclean -C ./libs/libft
	@$(MAKE) fclean -C ./libs/get_next_line
	@rm -f $(NAME)
	@rm -f $(MLX)
	@echo "FCLEAN DONE"

re: fclean all

.PHONY: all clean fclean re bonus
